import React from 'react';
import Movie from './movie'; 
import Ad from './Ad';
import Info from './Info';
import Header from './Header';
function App() {
    const poster = "./src/assets/bayn_bool.jpg";
    const rating = 8.66;
    const adPhotos = [
        "./src/assets/100kino.png",
        "./src/assets/hamgiin_saihan2.png",
    ];

    return (
        <div>
            <Header/>
            <Ad photos={adPhotos} />
            <Movie poster={poster} rating={rating} />
            <Info />
        </div>
    );
}

export default App;
