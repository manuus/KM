import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './Ad.css';

function Ad({ photos }) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  };

  return (
    <Slider {...settings}>
      {photos.map((photo, index) => (
        <div key={index} className="ad-slide">
          <img src={photo} alt={`Advertisement ${index + 1}`} className="ad-photo" />
        </div>
      ))}
    </Slider>
  );
}

export default Ad;
