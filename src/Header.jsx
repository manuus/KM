import './styles/Header.css'
import logo from './assets/kanumanulogo.svg'
function Header(){
    return(  
        <header>
            <div className="navigationSkeleton"></div>
             <nav>
                <div className="flex">
                <img src={logo} alt="" style={{height: '30px'}} />
                <ul>
                    <li><a href="">Кино</a></li>
                    <li><a href="">Авдар</a></li>
                    <li><a href="">Уран бүтээлчид</a></li>
                    <li className="puzzle"><a href="">Таавар</a></li>
                    
                </ul>
                </div>
                
                <ul className="login-search">
                    <li>
                            <input className="searchInput" type="search" />
                            <div className="searchIcon"><i class="fa fa-search"/></div>
                    </li>
                    <li className="loginButton">
                    <a >Нэвтрэх</a>
                    </li>
                </ul>
            </nav>
        </header>
           
    
            
            
    )
}

export default Header