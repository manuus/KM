import React from "react";
import './info.css';

const Info = () => {
    return (
        <div className="info-container">
            <div className="info-item">
                <img className="info-image" src="src/assets/png/mongol.png" alt="tochka2" />
                    <p className="info-text">37-р Точка киноны 3р бүлэг энэ оны 8 сард гарахаар зарлагдлаа. Энэ нь 650 сая төгрөгний төсөвтэйгээр төлөвлөгдсөн.</p>
                    <p className="info-date">2024-04-09</p>
            </div>
            <div className="info-item">
                <img className="info-image" src="src/assets/png/tochka.png" alt="tochka2" />
                    <p className="info-text">37-р Точка киноны 3р бүлэг энэ оны 8 сард гарахаар зарлагдлаа. Энэ нь 650 сая төгрөгний төсөвтэйгээр төлөвлөгдсөн.</p>
                    <p className="info-date">2024-04-09</p>
            </div>
            <div className="info-item">
                <img className="info-image" src="src/assets/png/tochka.png" alt="tochka2" />
                    <p className="info-text">37-р Точка киноны 3р бүлэг энэ оны 8 сард гарахаар зарлагдлаа. Энэ нь 650 сая төгрөгний төсөвтэйгээр төлөвлөгдсөн.</p>
                    <p className="info-date">2024-04-09</p>
            </div>
          
        </div>
    );
};

export default Info;
