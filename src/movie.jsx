import React from 'react';
import Hover from './hover'
import './Movie.css';

function Movie({ poster, rating }) {
  return (
    <div className="movie">
      <img src={poster} alt="Movie Poster" className="movie-poster"  style={{ width: '230px', height: '300px' }}/>
      <div className="rating">{rating}</div>
      <div className="additional-text"><Hover /></div>
    </div>
  );
}

export default Movie;
