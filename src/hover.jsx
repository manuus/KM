import React from 'react';
import './Movie.css';

function Hover() {

    const movie = {
        title: 'Баян боол',
        age: 'R+',
        listedNumber: 204,
        reviewNumber: 1502,
        genre: 'Адал явдалт  Инээдэм  Гэр бүлийн',
        info: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s',
        year: 2023, 
        content: 'Кино',
        time: '120мин'
    };

    return (
        <div className="hover_main">
            <p className="hover_title">{movie.title}</p>
            <div className='hover_sub'>
                <div className='border'>{movie.age}</div>
                <div className="addone">
                    <div>{movie.listedNumber}</div>
                    <div className="addtwo">{movie.reviewNumber}</div>
                </div>
            </div>
            <p className="hover_genre">{movie.genre}</p>
            <p className="hover_info">{movie.info}</p>
            <div className="hover_bottom">
                <div>{movie.year}</div>
                <div className='border'>{movie.content}</div>
                <div>{movie.time}</div>
            </div>
        </div>
    );
}

export default Hover;